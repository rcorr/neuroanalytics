#/!/bin/bash

##for donor in range 1-100
for donor in `seq 1 100`
do
	#adds leading zeros to donor
	donornum=$( printf '%03d' $donor )    
	#for time point in range 1-10
	for tp in `seq 1 10`
	do    
	        #adds leading zeros to tp
	        tpnum=$( printf '%03d' $tp ) 
	        #create filename based on donor and timepoint with "data" header
		FILENAME="donor${donornum}_tp${tpnum}.txt"
		echo "data" > $FILENAME
		#appends 5 random numbers to FILENAME after "data" header
		for i in `seq 1 5`
		do
			echo $RANDOM >> $FILENAME
		done
	done
done
	   
