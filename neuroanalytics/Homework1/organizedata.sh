#/!/bin/bash

##creates directory "fakedata"
FakeDataDir="fakedata"
mkdir $FakeDataDir

##for donor in range 1-50
for donornum in `seq 1 50`
do
	##makes directory for donor
	DonorDir="donor${donornum}"
	mkdir $DonorDir
	##for time point in range 1-10 moves file to donor directory
	for tpnum in `seq 1 10`
	do
		##makes FILENAME (based on donor and timepoint) read only and moves it to DonorDir 
		FILENAME="donor${donornum}_tp${tpnum}.txt"
		chmod a=r $FILENAME
		mv $FILENAME $DonorDir
	done
	##moves DonorDir into fakedata directory
	mv $DonorDir $FakeDataDir
done



