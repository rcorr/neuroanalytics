#/!/bin/bash

##for donor in range 1-50
for donornum in `seq 1 50`
do
	#for time point in range 1-10
	for tpnum in `seq 1 10`
	do
		#create filename based on donor and timepoint with "data" header
		FILENAME="donor${donornum}_tp${tpnum}.txt"
		echo "data" > $FILENAME
		#appends 5 random numbers to FILENAME after "data" header
		for i in `seq 1 5`
		do
			echo $RANDOM >> $FILENAME
		done
	done
done