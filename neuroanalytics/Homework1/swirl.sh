#!/bin/bash

# #loops the reversed spinner 10 times
# for i in `seq 1 10`
# do
# 	##with backspace interpretor and trailing new line omitted echos '/' then sleeps .25 seconds
# 	echo -ne '/';
# 	sleep .25;
# 	##with backspace interpretor and trailing new line omitted backspaces prior character then echos '|' then sleeps .25 seconds
# 	echo -ne '\b|';
# 	sleep .25;
# 	##with backspace interpretor and trailing new line omitted backspaces prior character then echos '\' then sleeps .25 seconds
# 	echo -ne '\b\\'
# 	sleep .25;
# 	##with backspace interpretor and trailing new line omitted backspaces prior character then echos '-' then sleeps .25 seconds
# 	echo -ne '\b-';
# 	sleep .25;
# 	##with backspace interpretor and trailing new line omitted backspaces prior character then echos '/' then sleeps .25 seconds
# 	echo -ne '\b/';
# 	sleep .25;
# 		##with backspace interpretor and trailing new line omitted backspaces prior character then echos '|' then sleeps .25 seconds
# 	echo -ne '\b|';
# 	sleep .25;
# 	##with backspace interpretor and trailing new line omitted backspaces prior character then echos '\' then sleeps .25 seconds
# 	echo -ne '\b\\'
# 	sleep .25;
# 	##with backspace interpretor and trailing new line omitted backspaces prior character then echos '-' then sleeps .25 seconds
# 	echo -ne '\b-';
# 	sleep .25;
# 	##with backspace interpretor and trailing new line omitted backspaces prior character
# 	echo -ne "\b";
# done
# ##with backspace interpretor and trailing new line omitted backspaces prior character and starts new line
# echo -ne "\b \n";

##with backspace interpretor and trailing new line included echos '||||W||||' in first line and '||||W||||' (forming 8 columns with centered Ws) in the next then sleeps .5 seconds
echo -e "||||W|||| \n||||W||||";
sleep .5;

#loops the following 10 times
for i in `seq 1 10`
do
	##echos '/\/\M/\/\' over the first line and '\/\/M/\/\/' over the next (forming 4 diamonds with centered Ms) then sleeps .5 seconds
	echo -e "\r\033[2A/\\/\\M/\\/\\ \n\\/\\/M\\/\\/";
	sleep .5;
	##echos '----W----' over the first line and '----W----' over the next (forming 2 rows with centered Ws) then sleeps .5 seconds
	echo -e "\r\033[2A----W---- \n----W----";
	sleep .5;
	##echos '\/\/M/\/\/' over the first line and '/\/\M/\/\' over the next (forming 4 Xs with centered Ms) then sleeps .5 seconds
	echo -e "\r\033[2A\\/\\/M\\/\\/ \n/\\/\\M/\\/\\";
	sleep .5;
	##echos '||||W||||' over the first line and '||||W||||' over the next (forming 8 columns with centered Ws) then sleeps .5 seconds
	echo -e "\r\033[2A||||W|||| \n||||W||||";
	sleep .5;
done
##erases second to last line
echo -e "\r\033[2A\033[0K";
##erases last line
echo -e "\r\033[1A\033[0K";